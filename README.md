

create mongo db + secret
1. `kubectl apply -f mongo-secret.yaml`
2. `kubectl apply -f mongo.yaml`

then create mongo express

3. `kubectl apply -f mongo-configmap.yaml`
4. `kubectl apply -f mongo-express.yaml`

to expose the external IP address to reach the External Service (aka LoadBalancer)

5. `minikube service mongo-express-service` # this opens a tunnel to the external service